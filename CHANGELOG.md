## <small>1.6.9 (2024-06-02)</small>

* 🔖 tag(package.json): v1.6.9 ([e17bb86](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/e17bb86))
* 🔨 script(setup.sh): update apt cmd ([1753b6e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/1753b6e))



## <small>1.6.8 (2024-05-28)</small>

* 📝 docs: update doc file ([b3fda6f](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/b3fda6f))
* 📝 docs(CHANGELOG.md): automatic update ([e45c3b8](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/e45c3b8))
* 🔖 tag(package.json): v1.6.8 ([fa25302](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/fa25302))



## <small>1.6.7 (2024-05-28)</small>

* 📝 docs: update doc file ([f02a47e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/f02a47e))
* 📝 docs(CHANGELOG.md): automatic update ([d862a87](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/d862a87))
* 🔖 tag(package.json): v1.6.7 ([d949642](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/d949642))



## <small>1.6.6 (2024-05-24)</small>

* 📝 docs(CHANGELOG.md): automatic update ([1cdae68](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/1cdae68))
* 🔖 tag(package.json): v1.6.6 ([d798a65](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/d798a65))



## <small>1.6.5 (2024-05-24)</small>

* ♻️ refactor(setup.sh): change how the deb package is installed ([471f55c](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/471f55c))
* 📝 docs(CHANGELOG.md): automatic update ([8135b72](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/8135b72))
* 🔖 tag(package.json): v1.6.5 ([d434753](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/d434753))



## <small>1.6.4 (2024-05-18)</small>

* 📝 docs(CHANGELOG.md): automatic update ([68dc59d](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/68dc59d))
* 🔖 tag(package.json): v1.6.4 ([300514d](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/300514d))



## <small>1.6.3 (2024-05-18)</small>

* ♻️ refactor(setup.sh): refactor ([e7772f9](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/e7772f9))
* 📝 docs: update doc file ([2e0f89e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2e0f89e))
* 📝 docs(CHANGELOG.md): automatic update ([f37feb9](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/f37feb9))
* 🔖 tag(package.json): v1.6.3 ([8f3a999](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/8f3a999))



## <small>1.6.2 (2024-05-18)</small>

* 📝 docs(CHANGELOG.md): automatic update ([837090d](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/837090d))
* 🔖 tag(package.json): v1.6.2 ([98992da](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/98992da))



## <small>1.6.1 (2024-05-18)</small>

* ♻️ refactor(setup.sh): refactor ([a5bb305](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/a5bb305))
* 📝 docs(CHANGELOG.md): automatic update ([b57a224](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/b57a224))
* 🔖 tag(package.json): v1.6.1 ([c02e7dc](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/c02e7dc))



## 1.6.0 (2024-05-18)

* 📝 docs(CHANGELOG.md): automatic update ([f4956c1](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/f4956c1))
* 🔖 tag(package.json): v1.6.0 ([afc86bd](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/afc86bd))



## <small>1.5.9 (2024-05-18)</small>

* 💡 comment(setup.sh): update comment ([863ee77](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/863ee77))
* 📝 docs(CHANGELOG.md): automatic update ([611c191](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/611c191))
* 🔖 tag(package.json): v1.5.9 ([d6b8e55](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/d6b8e55))



## <small>1.5.8 (2024-05-11)</small>

* 📝 docs(CHANGELOG.md): automatic update ([12eabd3](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/12eabd3))
* 🔖 tag(package.json): v1.5.8 ([0383960](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/0383960))



## <small>1.5.7 (2024-05-11)</small>

* 📝 docs: update doc file ([c58f375](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/c58f375))
* 📝 docs(CHANGELOG.md): automatic update ([7af05bd](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/7af05bd))
* 🔖 tag(package.json): v1.5.7 ([5f209d2](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/5f209d2))



## <small>1.5.6 (2024-05-11)</small>

* 📝 docs(CHANGELOG.md): automatic update ([7b202ca](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/7b202ca))
* 🔖 tag(package.json): v1.5.6 ([475d621](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/475d621))



## <small>1.5.5 (2024-05-11)</small>

* 📝 docs: update doc file ([32f6bf8](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/32f6bf8))
* 📝 docs(CHANGELOG.md): automatic update ([918e997](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/918e997))
* 🔖 tag(package.json): v1.5.5 ([b9f16be](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/b9f16be))



## <small>1.5.4 (2024-05-11)</small>

* 📝 docs(CHANGELOG.md): automatic update ([b6eb81d](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/b6eb81d))
* 🔖 tag(package.json): v1.5.4 ([5164e8d](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/5164e8d))



## <small>1.5.3 (2024-05-11)</small>

* ⏪ revert(hdimage.h): uncomment some code ([1e0a391](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/1e0a391))
* ✨ feat(setup.sh): add logic to rm share dir ([f2249bc](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/f2249bc))
* 📝 docs: update doc file ([b4e987f](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/b4e987f))
* 📝 docs(CHANGELOG.md): automatic update ([a3212fe](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/a3212fe))
* 🔖 tag(package.json): v1.5.3 ([87f8f88](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/87f8f88))



## <small>1.5.2 (2024-05-11)</small>

* 📝 docs(CHANGELOG.md): automatic update ([7f538d6](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/7f538d6))
* 🔖 tag(package.json): v1.5.2 ([42a9bff](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/42a9bff))



## <small>1.5.1 (2024-05-11)</small>

* 📝 docs: update doc file ([963b091](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/963b091))
* 📝 docs(CHANGELOG.md): automatic update ([1bdec71](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/1bdec71))
* 🔖 tag(package.json): v1.5.1 ([1c76470](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/1c76470))



## 1.5.0 (2024-05-11)

* 📝 docs(CHANGELOG.md): automatic update ([49e7254](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/49e7254))
* 🔖 tag(package.json): v1.5.0 ([2fbb076](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2fbb076))



## <small>1.4.9 (2024-05-11)</small>

* 📝 docs: update doc file ([b141341](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/b141341))
* 📝 docs(CHANGELOG.md): automatic update ([7ff869c](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/7ff869c))
* 🔖 tag(package.json): v1.4.9 ([2395436](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2395436))



## <small>1.4.8 (2024-05-11)</small>

* 📝 docs(CHANGELOG.md): automatic update ([f4d15a9](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/f4d15a9))
* 🔖 tag(package.json): v1.4.8 ([5f77988](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/5f77988))



## <small>1.4.7 (2024-05-11)</small>

* ♻️ refactor(mount.sh): shellcheck ([9687a63](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/9687a63))
* ♻️ refactor(run.sh): shellcheck ([2729996](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2729996))
* ♻️ refactor(score.sh): shellcheck ([1ae589c](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/1ae589c))
* ♻️ refactor(setup.sh): shellcheck ([5007501](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/5007501))
* ♻️ refactor(umount.sh): shellcheck ([f51b746](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/f51b746))
* 📝 docs(CHANGELOG.md): automatic update ([ebeb1ac](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/ebeb1ac))
* 🔖 tag(package.json): v1.4.7 ([e3e0cf0](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/e3e0cf0))



## <small>1.4.6 (2024-05-11)</small>

* 📝 docs: update doc file ([ed718f8](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/ed718f8))
* 📝 docs(CHANGELOG.md): automatic update ([6a4241b](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/6a4241b))
* 🔖 tag(package.json): v1.4.6 ([cfe8856](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/cfe8856))



## <small>1.4.5 (2024-05-10)</small>

* 📝 docs(CHANGELOG.md): automatic update ([992d0ce](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/992d0ce))
* 🔖 tag(package.json): v1.4.5 ([2fe72af](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2fe72af))



## <small>1.4.4 (2024-05-10)</small>

* 📝 docs(CHANGELOG.md): automatic update ([36d136e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/36d136e))
* 🔖 tag(package.json): v1.4.4 ([2764f5d](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2764f5d))



## <small>1.4.3 (2024-05-10)</small>

* 📝 docs(CHANGELOG.md): automatic update ([74d8f00](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/74d8f00))
* 🔖 tag(package.json): v1.4.3 ([5d77101](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/5d77101))



## <small>1.4.2 (2024-05-10)</small>

* 📝 docs(CHANGELOG.md): automatic update ([8ad7e04](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/8ad7e04))
* 🔖 tag(package.json): v1.4.2 ([87c5a44](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/87c5a44))



## <small>1.4.1 (2024-05-10)</small>

* 📝 docs(CHANGELOG.md): automatic update ([47c5941](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/47c5941))
* 🔖 tag(package.json): v1.4.1 ([a4a1d4e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/a4a1d4e))



## 1.4.0 (2024-05-10)

* 📝 docs(CHANGELOG.md): automatic update ([a6465e6](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/a6465e6))
* 🔖 tag(package.json): v1.4.0 ([e4fdeae](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/e4fdeae))



## <small>1.3.9 (2024-05-10)</small>

* 📝 docs(CHANGELOG.md): automatic update ([c7e0cb9](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/c7e0cb9))
* 🔖 tag(package.json): v1.3.9 ([74cbe0c](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/74cbe0c))



## <small>1.3.8 (2024-05-10)</small>

* 📝 docs: update doc file ([579271c](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/579271c))
* 📝 docs(CHANGELOG.md): automatic update ([12ae0fa](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/12ae0fa))
* 🔖 tag(package.json): 1.3.8 ([3c28f30](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/3c28f30))



## <small>1.3.7 (2024-05-09)</small>

* 📝 docs(CHANGELOG.md): automatic update ([fe7a9ed](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/fe7a9ed))
* 🔖 tag(package.json): 1.3.7 ([3887cd5](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/3887cd5))



## <small>1.3.6 (2024-05-09)</small>

* 📝 docs(CHANGELOG.md): automatic update ([025b3b4](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/025b3b4))
* 🔖 tag(package.json): 1.3.6 ([2208063](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2208063))



## <small>1.3.5 (2024-05-09)</small>

* 📝 docs(CHANGELOG.md): automatic update ([ff3b41f](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/ff3b41f))
* 🔖 tag(package.json): 1.3.4 ([4c7d963](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/4c7d963))



## <small>1.3.3 (2024-05-09)</small>

* ⚡️ perf(setup.sh): change download url to mirror station ([e25fc90](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/e25fc90))
* ⚡️ perf(setup.sh): change download url to mirror station ([ec8311d](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/ec8311d))
* ➕ add_dep: add bochs-2.2.6 src code ([5fee0b1](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/5fee0b1))
* ➕ add_dep(bochs-2.2.5): add bochs-2.2.5 ([2c7a332](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2c7a332))
* ➖ rm_dep(bochs-2.2.6): rm bochs-2.2.6 ([092b443](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/092b443))
* 🎨 style(setup.sh): format ([459b856](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/459b856))
* 🎨 style(setup.sh): format ([518ecac](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/518ecac))
* 🐛 fix: m0rtzz have resolved the bug in the source code ([25dcdff](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/25dcdff))
* 🐛 fix(cpu.cc): fix bug ([f4d0598](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/f4d0598))
* 🐛 fix(cpu.cc): fix bug ([d1cb818](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/d1cb818))
* 💡 comment: update comment ([c944dfa](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/c944dfa))
* 💡 comment(cpu.cc): update comment ([9486598](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/9486598))
* 💡 comment(mount.sh): update comment ([828a2e6](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/828a2e6))
* 💡 comment(socket_server.c): update comment ([7eebecc](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/7eebecc))
* 💡 comment(socket_server.c): update comment ([0a6ba7b](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/0a6ba7b))
* 💡 comment(socket_server.c): update comment ([2946ea6](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2946ea6))
* 💩 poop: shit ([ffe8499](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/ffe8499))
* 📝 docs: update ([5cafcbc](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/5cafcbc))
* 📝 docs: update ([606a6ac](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/606a6ac))
* 📝 docs: update ([a5bd861](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/a5bd861))
* 📝 docs: update ([d4ebbb6](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/d4ebbb6))
* 📝 docs: update ([d3e3064](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/d3e3064))
* 📝 docs: update ([404e766](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/404e766))
* 📝 docs: update ([4ec70af](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/4ec70af))
* 📝 docs: update ([5d14133](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/5d14133))
* 📝 docs: update doc ([a8e43dc](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/a8e43dc))
* 📝 docs: update doc ([c09507e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/c09507e))
* 📝 docs: update doc ([4123c8f](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/4123c8f))
* 📝 docs: update doc ([3b88640](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/3b88640))
* 📝 docs: update doc ([97b551e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/97b551e))
* 📝 docs: update doc ([581752e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/581752e))
* 📝 docs: update doc file ([24f4b50](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/24f4b50))
* 📝 docs: update doc file ([fe7f0d9](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/fe7f0d9))
* 📝 docs: update doc file ([b5cae32](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/b5cae32))
* 📝 docs: update doc file ([9ada7ce](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/9ada7ce))
* 📝 docs: update doc file ([5c677e2](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/5c677e2))
* 📝 docs: update doc file ([b968191](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/b968191))
* 📝 docs: update doc file ([5215840](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/5215840))
* 📝 docs: update doc file ([987c6c3](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/987c6c3))
* 📝 docs(CHANGELOG.md): automatic update ([77a06b7](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/77a06b7))
* 📝 docs(CHANGELOG.md): automatic update ([98745ab](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/98745ab))
* 📝 docs(CHANGELOG.md): automatic update ([69567e2](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/69567e2))
* 📝 docs(CHANGELOG.md): automatic update ([5d3d3ba](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/5d3d3ba))
* 📝 docs(CHANGELOG.md): automatic update ([dec28b7](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/dec28b7))
* 📝 docs(CHANGELOG.md): automatic update ([c98ab11](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/c98ab11))
* 📝 docs(CHANGELOG.md): automatic update ([1cd79e8](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/1cd79e8))
* 📝 docs(CHANGELOG.md): automatic update ([416f148](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/416f148))
* 📝 docs(CHANGELOG.md): automatic update ([4d4f57c](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/4d4f57c))
* 📝 docs(CHANGELOG.md): automatic update ([ee62a2e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/ee62a2e))
* 📝 docs(CHANGELOG.md): automatic update ([2235c29](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2235c29))
* 📝 docs(CHANGELOG.md): automatic update ([202db90](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/202db90))
* 📝 docs(CHANGELOG.md): automatic update ([b7d7d81](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/b7d7d81))
* 📝 docs(CHANGELOG.md): automatic update ([2f4062a](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2f4062a))
* 📝 docs(CHANGELOG.md): automatic update ([fa3a9ba](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/fa3a9ba))
* 📝 docs(CHANGELOG.md): automatic update ([f08f834](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/f08f834))
* 📝 docs(CHANGELOG.md): automatic update ([f2f0642](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/f2f0642))
* 📝 docs(CHANGELOG.md): automatic update ([b672949](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/b672949))
* 📝 docs(CHANGELOG.md): automatic update ([72e2c8e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/72e2c8e))
* 📝 docs(CHANGELOG.md): automatic update ([f871bc4](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/f871bc4))
* 📝 docs(CHANGELOG.md): automatic update ([3f3e755](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/3f3e755))
* 📝 docs(CHANGELOG.md): automatic update ([6103b2c](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/6103b2c))
* 📝 docs(CHANGELOG.md): automatic update ([ababe28](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/ababe28))
* 📝 docs(CHANGELOG.md): automatic update ([9955a71](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/9955a71))
* 📝 docs(CHANGELOG.md): automatic update ([9d21d83](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/9d21d83))
* 📝 docs(CHANGELOG.md): automatic update ([c596669](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/c596669))
* 📝 docs(CHANGELOG.md): automatic update ([e06aafc](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/e06aafc))
* 📝 docs(CHANGELOG.md): automatic update ([3cee25e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/3cee25e))
* 📝 docs(CHANGELOG.md): automatic update ([dda3db4](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/dda3db4))
* 📝 docs(CHANGELOG.md): automatic update ([0b2ee1d](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/0b2ee1d))
* 📝 docs(CHANGELOG.md): automatic update ([6310958](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/6310958))
* 📝 docs(CHANGELOG.md): automatic update ([c3eb2e7](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/c3eb2e7))
* 📝 docs(CHANGELOG.md): automatic update ([4b266fd](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/4b266fd))
* 📝 docs(CHANGELOG.md): automatic update ([3e7019e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/3e7019e))
* 📝 docs(CHANGELOG.md): automatic update ([7567679](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/7567679))
* 📝 docs(CHANGELOG.md): automatic update ([44e33e6](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/44e33e6))
* 📝 docs(CHANGELOG.md): automatic update ([0de66c0](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/0de66c0))
* 📝 docs(CHANGELOG.md): automatic update ([162403c](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/162403c))
* 📝 docs(CHANGELOG.md): automatic update ([62e5782](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/62e5782))
* 📝 docs(CHANGELOG.md): automatic update ([64fcd0c](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/64fcd0c))
* 📝 docs(CHANGELOG.md): automatic update ([0635633](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/0635633))
* 📝 docs(CHANGELOG.md): automatic update ([e209c85](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/e209c85))
* 📝 docs(CHANGELOG.md): automatic update ([0ee401c](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/0ee401c))
* 📝 docs(CHANGELOG.md): automatic update ([5dc8f47](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/5dc8f47))
* 📝 docs(CHANGELOG.md): automatic update ([17bfddb](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/17bfddb))
* 📝 docs(CHANGELOG.md): automatic update ([7322ad1](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/7322ad1))
* 📝 docs(CHANGELOG.md): automatic update ([0926aa9](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/0926aa9))
* 📝 docs(CHANGELOG.md): automatic update ([9707332](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/9707332))
* 📝 docs(CHANGELOG.md): automatic update ([970056f](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/970056f))
* 📝 docs(CHANGELOG.md): automatic update ([4b7b2ea](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/4b7b2ea))
* 📝 docs(CHANGELOG.md): automatic update ([3423a19](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/3423a19))
* 📝 docs(CHANGELOG.md): automatic update ([cc00764](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/cc00764))
* 📝 docs(CHANGELOG.md): automatic update ([d5806ef](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/d5806ef))
* 📝 docs(CHANGELOG.md): automatic update ([332a3ec](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/332a3ec))
* 📝 docs(CHANGELOG.md): automatic update ([cb7a6f4](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/cb7a6f4))
* 📝 docs(CHANGELOG.md): automatic update ([0b480df](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/0b480df))
* 📝 docs(CHANGELOG.md): automatic update ([8035cab](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/8035cab))
* 📝 docs(CHANGELOG.md): automatic update ([a5fc722](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/a5fc722))
* 📝 docs(README.md): update ([db8e39a](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/db8e39a))
* 📝 docs(README.md): update doc ([0fe3e8c](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/0fe3e8c))
* 🔖 tag(package.json): 1.1.9 ([0b283af](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/0b283af))
* 🔖 tag(package.json): 1.1.9 ([684179e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/684179e))
* 🔖 tag(package.json): 1.2.0 ([0f240e6](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/0f240e6))
* 🔖 tag(package.json): 1.2.0 ([d523f29](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/d523f29))
* 🔖 tag(package.json): 1.2.1 ([cc6b022](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/cc6b022))
* 🔖 tag(package.json): 1.2.1 ([9142a40](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/9142a40))
* 🔖 tag(package.json): 1.2.2 ([9cbdfbb](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/9cbdfbb))
* 🔖 tag(package.json): 1.2.2 ([d10e5c2](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/d10e5c2))
* 🔖 tag(package.json): 1.2.3 ([23c48bc](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/23c48bc))
* 🔖 tag(package.json): 1.2.3 ([fbc0259](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/fbc0259))
* 🔖 tag(package.json): 1.2.4 ([dda061a](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/dda061a))
* 🔖 tag(package.json): 1.2.4 ([3ba6c35](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/3ba6c35))
* 🔖 tag(package.json): 1.2.5 ([671dee4](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/671dee4))
* 🔖 tag(package.json): 1.2.5 ([2b12cc6](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2b12cc6))
* 🔖 tag(package.json): 1.2.6 ([39c490c](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/39c490c))
* 🔖 tag(package.json): 1.2.6 ([818bc21](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/818bc21))
* 🔖 tag(package.json): 1.2.7 ([8c96c7b](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/8c96c7b))
* 🔖 tag(package.json): 1.2.7 ([ad47830](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/ad47830))
* 🔖 tag(package.json): 1.2.8 ([1a9aa21](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/1a9aa21))
* 🔖 tag(package.json): 1.2.8 ([cc3ee08](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/cc3ee08))
* 🔖 tag(package.json): 1.2.9 ([2eee103](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2eee103))
* 🔖 tag(package.json): 1.2.9 ([e626a57](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/e626a57))
* 🔖 tag(package.json): 1.3.0 ([9046a50](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/9046a50))
* 🔖 tag(package.json): 1.3.0 ([9779f4b](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/9779f4b))
* 🔖 tag(package.json): 1.3.1 ([a0c6ed5](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/a0c6ed5))
* 🔖 tag(package.json): 1.3.1 ([cee739d](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/cee739d))
* 🔖 tag(package.json): 1.3.2 ([aa719a9](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/aa719a9))
* 🔖 tag(package.json): 1.3.2 ([dfea6b5](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/dfea6b5))
* 🔖 tag(package.json): 1.3.3 ([c97d670](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/c97d670))
* 🔖 tag(package.json): 1.3.3 ([ff8f4c8](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/ff8f4c8))
* 🔖 tag(package.json): 1.3.4 ([5fa9d5d](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/5fa9d5d))
* 🔖 tag(package.json): 1.3.5 ([6c18e55](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/6c18e55))
* 🔖 tag(package.json): 1.3.6 ([a3840be](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/a3840be))
* 🔖 tag(package.json): 1.3.7 ([2055823](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2055823))
* 🔖 tag(package.json): 1.3.8 ([52a77b4](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/52a77b4))
* 🔖 tag(package.json): 1.3.9 ([d8b0942](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/d8b0942))
* 🔖 tag(package.json): 1.4.0 ([8983bb8](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/8983bb8))
* 🔖 tag(package.json): 1.4.1 ([8818ecb](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/8818ecb))
* 🔖 tag(package.json): 1.4.2 ([4a43fa6](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/4a43fa6))
* 🔖 tag(package.json): 1.4.3 ([bc6a51a](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/bc6a51a))
* 🔖 tag(package.json): 1.4.4 ([fdf3eaf](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/fdf3eaf))
* 🔖 tag(package.json): 1.4.5 ([85cc06e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/85cc06e))
* 🔖 tag(package.json): 1.4.6 ([b28ba5b](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/b28ba5b))
* 🔖 tag(package.json): 1.4.7 ([9ffabcc](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/9ffabcc))
* 🔖 tag(package.json): 1.4.8 ([f0a2ea6](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/f0a2ea6))
* 🔖 tag(package.json): 1.4.9 ([4de6f2d](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/4de6f2d))
* 🔖 tag(package.json): 1.5.0 ([147fe16](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/147fe16))
* 🔖 tag(package.json): 1.5.1 ([79aea38](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/79aea38))
* 🔖 tag(package.json): 1.5.2 ([8b75acb](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/8b75acb))
* 🔖 tag(package.json): 1.5.3 ([73c5182](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/73c5182))
* 🔖 tag(package.json): 1.5.4 ([06be5f2](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/06be5f2))
* 🔖 tag(package.json): 1.5.5 ([56c8bc9](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/56c8bc9))
* 🔖 tag(package.json): 1.6.3 ([99b05c1](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/99b05c1))
* 🔖 tag(package.json): 1.6.9 ([cb611eb](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/cb611eb))
* 🔖 tag(package.json): 1.7.0 ([dfd9d89](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/dfd9d89))
* 🔖 tag(package.json): 1.7.1 ([cd3938b](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/cd3938b))
* 🔖 tag(package.json): 1.7.2 ([3619399](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/3619399))
* 🔥 remove: rm exe file ([183d4d1](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/183d4d1))
* 🔥 remove: rm log file ([0b04333](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/0b04333))
* 🔥 remove: rm log file ([bc22446](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/bc22446))
* 🔥 remove: rm unused dir ([42b4dfd](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/42b4dfd))
* 🔥 remove: rm unused file ([8adcbfa](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/8adcbfa))
* 🔥 remove: rm unused file ([5e1530e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/5e1530e))
* 🔥 remove(dbg.c): rm unused src code file ([4c6f0fd](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/4c6f0fd))
* 🔧 config: update run cmd files ([546b326](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/546b326))
* 🔨 script: format ([a45da15](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/a45da15))
* 🔨 script(setup.sh): after successful compilation, execute make install ([862808e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/862808e))
* 🔨 script(setup.sh): downgrade bochs version ([b7f841d](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/b7f841d))
* 🔨 script(setup.sh): make clean first ([ee9fa06](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/ee9fa06))
* 🔨 script(setup.sh): multithreaded compilation ([ab564b9](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/ab564b9))
* 🔨 script(setup.sh): rename func ([d481c49](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/d481c49))
* 🔨 script(setup.sh): update ([b579501](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/b579501))
* 🔨 script(setup.sh): update info msg ([e345f75](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/e345f75))
* 🔨 script(setup.sh): update info msg ([61e7192](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/61e7192))
* 🔨 script(setup.sh): update logic ([1fd7a9f](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/1fd7a9f))
* 🔨 script(setup.sh): update setup.sh ([82f7b42](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/82f7b42))
* 🔨 script(umount.sh): add logic to kill processes occupying hdc/ ([3413a1c](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/3413a1c))
* 🔨 script(umount.sh): change info msg ([6cba2d8](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/6cba2d8))
* 🙈 ignore: ignore the fking panic ([ce06d4b](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/ce06d4b))
* 🙈 ignore(.gitignore): add .a ([9a192ee](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/9a192ee))
* 🙈 ignore(.gitignore): add exe file ([7e12ef1](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/7e12ef1))
* 🙈 ignore(.gitignore): ignore exe file ([b41e3b4](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/b41e3b4))
* 🙈 ignore(.gitignore): ignore unused src code file ([9302cfa](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/9302cfa))
* 🙈 ignore(.gitignore): rm unused docker dir ([b00d6f5](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/b00d6f5))
* 🙈 ignore(.gitignore): update ([4a03596](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/4a03596))
* 🙈 ignore(.gitignore): update .gitignore ([6b21020](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/6b21020))
* 🙈 ignore(.gitignore): update .gitignore ([bc31780](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/bc31780))
* 🙈 ignore(.gitignore): update .gitignore ([809c5b9](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/809c5b9))
* 🙈 ignore(.gitignore): update .gitignore ([353394a](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/353394a))
* 🚚 move: mv resources ([f65735f](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/f65735f))
* 🚚 move: mv resources ([12d1af2](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/12d1af2))
* 🚚 move: mv resources ([39cca85](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/39cca85))
* 🚚 move: rename doc file ([26225e6](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/26225e6))
* 🚚 move(linux-0.12-pure-unmodified.tar.gz): mv unmodified kernel src code path ([08db427](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/08db427))
* chore(release): 1.2.10 ([f878cb2](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/f878cb2))
* tag(package.json): 1.4.10 ([05d4f88](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/05d4f88))



## <small>1.1.8 (2024-05-03)</small>

* chore(release): 1.1.8 ([ef9b0b2](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/ef9b0b2))
* 📝 docs: update ([7e3ce1b](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/7e3ce1b))
* 📝 docs(CHANGELOG.md): automatic update ([9e0e5bb](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/9e0e5bb))



## <small>1.1.7 (2024-05-02)</small>

* chore(release): 1.1.7 ([6d2b98c](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/6d2b98c))
* 📝 docs: update ([74a4231](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/74a4231))
* 📝 docs(CHANGELOG.md): automatic update ([386c2ef](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/386c2ef))



## <small>1.1.6 (2024-05-02)</small>

* chore(release): 1.1.6 ([5abac5d](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/5abac5d))
* 📝 docs: update ([7265ae6](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/7265ae6))
* 📝 docs(CHANGELOG.md): automatic update ([dc59f70](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/dc59f70))



## <small>1.1.5 (2024-05-02)</small>

* chore(release): 1.1.5 ([575ed92](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/575ed92))
* 📝 docs: update ([2362f91](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2362f91))
* 📝 docs(CHANGELOG.md): automatic update ([812bef6](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/812bef6))



## <small>1.1.4 (2024-05-02)</small>

* chore(release): 1.1.4 ([a4b7b2c](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/a4b7b2c))
* 📝 docs: update ([3dd0331](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/3dd0331))
* 📝 docs(CHANGELOG.md): automatic update ([84ff7dc](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/84ff7dc))



## <small>1.1.3 (2024-05-02)</small>

* chore(release): 1.1.3 ([dae9129](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/dae9129))
* 💡 comment(pv.c): add comment ([65d9d72](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/65d9d72))
* 📝 docs: update ([d6cd666](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/d6cd666))
* 📝 docs(CHANGELOG.md): automatic update ([4704bb5](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/4704bb5))



## <small>1.1.2 (2024-05-02)</small>

* chore(release): 1.1.2 ([61eb5e4](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/61eb5e4))
* 📝 docs(CHANGELOG.md): automatic update ([6a4e231](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/6a4e231))
* 📝 docs(README.pdf): update pdf file ([a497452](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/a497452))



## <small>1.1.1 (2024-05-02)</small>

* chore(release): 1.1.1 ([e1f68f5](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/e1f68f5))
* ✨ feat: update ([60cd6bb](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/60cd6bb))
* 📝 docs(CHANGELOG.md): automatic update ([1d8bb7c](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/1d8bb7c))
* 📝 docs(README.md): update README.md ([74f9966](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/74f9966))



## 1.1.0 (2024-05-02)

* chore(release): 1.0.10 ([2f0901f](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2f0901f))
* 📝 docs(CHANGELOG.md): automatic update ([363acec](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/363acec))
* 📝 docs(README.md): update README.md ([dd52e76](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/dd52e76))



## <small>1.0.9 (2024-05-02)</small>

* chore(release): 1.0.9 ([7ac69aa](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/7ac69aa))
* 📝 docs(CHANGELOG.md): automatic update ([b81d59b](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/b81d59b))
* 📝 docs(README.pdf): update pdf file ([9110560](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/9110560))



## <small>1.0.8 (2024-05-02)</small>

* chore(release): 1.0.8 ([2d82d42](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2d82d42))
* 📝 docs(CHANGELOG.md): automatic update ([797d6e0](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/797d6e0))
* 🔨 script(score.sh): update ([a16b66f](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/a16b66f))



## <small>1.0.7 (2024-05-02)</small>

* chore(release): 1.0.7 ([c848262](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/c848262))
* 📝 docs(CHANGELOG.md): automatic update ([ea3114e](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/ea3114e))



## <small>1.0.6 (2024-05-02)</small>

* chore(release): 1.0.6 ([43ecf8b](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/43ecf8b))
* 📝 docs(CHANGELOG.md): automatic update ([a644835](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/a644835))



## <small>1.0.5 (2024-05-02)</small>

* chore(release): 1.0.5 ([756b645](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/756b645))
* 📝 docs(CHANGELOG.md): automatic update ([43200ea](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/43200ea))



## <small>1.0.4 (2024-05-02)</small>

* chore(release): 1.0.4 ([dd8143a](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/dd8143a))
* 📝 docs(CHANGELOG.md): automatic update ([d54705d](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/d54705d))



## <small>1.0.3 (2024-05-02)</small>

* chore(release): 1.0.3 ([1fb61a9](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/1fb61a9))
* ✨ feat: update ([2d36acf](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/2d36acf))
* 📝 docs(CHANGELOG.md): automatic update ([cea64cb](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/cea64cb))



## <small>1.0.2 (2024-05-02)</small>

* chore(release): 1.0.2 ([e435941](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/e435941))
* 🍱 asset(hdc.tar.xz): add hdc.tar.xz ([6cec489](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/6cec489))
* 📝 docs(CHANGELOG.md): add CHANGELOG.md ([11d9490](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/11d9490))
* 📝 docs(CHANGELOG.md): automatic update ([7fddeca](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/7fddeca))



## <small>1.0.1 (2024-05-02)</small>

* chore(release): 1.0.1 ([5c36649](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/5c36649))
* 📝 docs(CHANGELOG.md): automatic update ([85ef433](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/85ef433))
* 🔥 remove(score.sh): rm unused comment ([3ee74df](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/3ee74df))



## 1.0.0 (2024-05-02)

* chore(release): 1.0.1 ([d447a9b](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/d447a9b))
* 🎉 init: init commit ([c7e9e28](https://gitcode.com/M0rtzz/zzu-cs-os-design/commits/detail/c7e9e28))



